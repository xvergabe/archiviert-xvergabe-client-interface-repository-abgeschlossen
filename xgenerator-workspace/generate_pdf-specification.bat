@echo off
SET projectname=xvergabe
SET fopdir=%CD%\fop-0.95
SET outdir=%CD%\pdf
SET specdir=%CD%\documentation
SET docbookxsldir=%CD%\docbookxsl\docbook-xsl-1.73.2
SET y=%date:~-4%
SET m=%date:~-7,2%
SET d=%date:~-10,2%

SET h=%time:~-11,2%
SET min=%time:~-8,2%
SET sek=%time:~-5,2%

SET datetag=%y%%m%%d%-%h%%min%%sek%
SET filename=%datetag%-%projectname%_specification.pdf


SET cmdtorun=%fopdir%\fop.bat -xml %specdir%\spezifikation.xml -xsl %docbookxsldir%\fo\docbook.xsl -pdf %outdir%\%filename%

CALL %cmdtorun%
PAUSE
