      _           _                           _   _          
     | |         (_)                         | | (_)         
  ___| | ___  ___ _ _ __   __ _   _ __   ___ | |_ _  ___ ___ 
 / __| |/ _ \/ __| | '_ \ / _` | | '_ \ / _ \| __| |/ __/ _ \
| (__| | (_) \__ \ | | | | (_| | | | | | (_) | |_| | (_|  __/
 \___|_|\___/|___/_|_| |_|\__, | |_| |_|\___/ \__|_|\___\___|
                           __/ |                             
                          |___/                              

Dieses Repository stellt einen historischen Entwicklungszweig 
der XVergabe-Kommunikationsschnittstelle dar und wird nicht 
mehr weiter gepflegt. Dieses Repository gilt somit als 
abgeschlossen. 
Fuer die aktuellen Entwicklungen wird ein neues Repository 
eingerichtet. 



This repository is a historic development branch of the 
XVergabe client interface which is not going to be updated 
any furhter. Thus this repository is being considered closed. 
For current developments a new repository is being created. 


          _   _                           _           _      
 ___ ___ (_)_| | ___   __ _   _ __   __ _(_)___  ___ | |___  
/ _ \__ \| |__ |/ _ \ / _` | | '_ \ / _` | |__ \/ _ \| |__ \ 
\__  |_) | |_| | (_) | | | | | |_) | | | | / __/ (_) | |__) |
|___/___/|_|__/ \___/|_| |_| | .__/|_| |_|_\___|\___/|_|___/ 
                              \___|      



